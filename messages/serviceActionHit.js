const stride = require("../strideClient");
const { Document } = require("adf-builder");

function createResponse(body) {
  let doc = new Document();
  doc.paragraph().text("The webhook received:");
  if (body.context.message) body.context.message.body = "Removed for brevity. See server side logs for more detail";
  doc.codeBlock("javascript").text(JSON.stringify(body, null, 2));
  return { body: doc.toJSON() };
}

module.exports = async function(cloudId, conversationId, body) {
  //send responses to the conversation
  await stride.api.messages.sendMessage(cloudId, conversationId, createResponse(body));
};
