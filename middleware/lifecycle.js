const express = require("express");
const router = express.Router({ mergeParams: true });
const auth = require("./auth")(process.env.CLIENT_SECRET);
const descriptor = require("../descriptor");

/**
 *  @name Lifecycle: app descriptor
 *  @see {@link https://developer.atlassian.com/cloud/stride/blocks/app-descriptor/ | Descriptor Requests }
 *  @see {@link https://developer.atlassian.com/cloud/stride/blocks/app-lifecycle/ | Lifecycle Events }
 *  @description
 *
 *  The app descriptor is a JSON file that describes how Stride should communicate with the app.
 *  The descriptor includes general information for the app, as well as the modules that the app wants to use.
 *  The app descriptor serves as the glue between the app and Stride. When a user installs an app,
 *  what they are really doing is installing this descriptor file.
 *
 *  Stride needs to be able to retrieve this from your app server.
 **/
router.get("/descriptor", function(req, res) {
  let output =  descriptor;
  output.baseUrl = `https://${req.headers.host}`;
  res.json(output);
});

/**
 *  @name Lifecycle: installation events
 *  @see {@link https://developer.atlassian.com/cloud/stride/blocks/app-lifecycle/ | Installation Events }
 *  @description
 *
 *  In order to be granted access to a conversation (for instance, to send messages) an app must be installed by a user in the conversation.
 *  Your app can be notified whenever a user installs or uninstalls it in a conversation. Stride makes a POST
 *  request that will be made to the lifecycle URL defined in the app descriptor.
 **/
router.post("/installed", auth, async (req, res, next) => {
  res.sendStatus(204);
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  };
  await require("../messages/messageWithActions")(context.cloudId, context.conversationId).catch(err => {
    console.error(`error sending message on install: ${err}`);
  });
  console.log(
    `installed:
     cloudId:\t${context.cloudId}
     conversationId:\t${context.conversationId}`
  );
});

/**
 *  @name Lifecycle: installation events
 *  @see {@link https://developer.atlassian.com/cloud/stride/blocks/app-lifecycle/ | Installation Events }
 *  @description
 *
 *  Apps can be uninstalled by a user. This is the even where your app will be notified of an uninstall.
 **/
router.post("/uninstalled", auth, async (req, res, next) => {
  res.sendStatus(204);
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  };
  console.log(
    `uninstalled:
     cloudId:${context.cloudId}
     conversationId:\t${context.conversationId}`
  );
});

module.exports = router;
